﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppClient
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonEnviar_Click(object sender, RoutedEventArgs e)
        {
            ServiceReferenceCliente.Service1Client service = new ServiceReferenceCliente.Service1Client();//Creamos el objeto del servicio 
            string nombre = TextBoxName.Text;//Pasamos el texto introducido a la variable
            if(!nombre.Equals(null))//Comprueba que no sea null
            {
                service.getName(nombre);//Metodo pasara el texto introducido a la parte del servidor
            }
            else
            {
                Console.WriteLine("Error no has puesto un nombre");
            }
        }
    }
}
