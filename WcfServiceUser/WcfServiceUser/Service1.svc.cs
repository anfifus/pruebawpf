﻿using System;

namespace WcfServiceUser
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        private bdExampleEntities modelo = new bdExampleEntities(); 
        
        public void getName(string userName)//Definimos la funcionalidad del metodo
        {
            Persona pers = new Persona();//Objeto descrito en el modelo que contiene unas caracteristicas
            pers.nombre = userName;//Añadimos dentro del objeto el texto que introducimos en el formulario recogido a partir de la comunicacion WCF
            modelo.Persona.Add(pers);//Añadimos el objeto creado dentro del modelo creado
            modelo.SaveChanges();//Añadimos los cambios del modelo a la base de datos
        }
    }
}
